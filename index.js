var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

var sock = null;

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.get('/:pay', function(req, res) {
  if (sock) {
    emit(req.params.pay);
  }
  res.send(req.params.pay);
});
io.on('connection', function(socket) {
  sock = socket;
  sock.on('pay', msg => emit(msg));
});

function emit(msg) {
  if (msg && msg.startsWith('favicon.ico')) return;
  io.emit('pay', msg);
}

http.listen(port, () => console.log(`Example app listening on port ${port}!`));
