# oto-pay-node

## How to start

``` sh
$ npm run start

> oto-pay-node@1.0.0 start /oto-pay-node
> node index.js

Example app listening on port 3000!

# 8080 port
$ PORT=8080 npm run start

> oto-pay-node@1.0.0 start /oto-pay-node
> node index.js

Example app listening on port 8080!
```

## How to use

```sh
$ curl localhost:3000/5000
5000

$ curl localhost:8080/1234
1234
```
